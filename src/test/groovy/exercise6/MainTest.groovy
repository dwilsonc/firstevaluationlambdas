package exercise6

import exercise5.models.Employee
import exercise5.utils.PopulateModel
import exercise6.utils.MyMath
import spock.lang.Specification


class MainTest extends Specification {

   def "Prueba que valida si es PAR/IMPAR"() {
        expect:
        boolean value = MyMath.isOdd(input);
        String resultado = (value) ? "ODD" : "EVEN"
        output == resultado;

        where:
        input | output
        4 | "EVEN"
        3 | "ODD"
    }

    def "Prueba que valida si es PRIMO/NO PRIMO"() {
        expect:
        boolean value = MyMath.isPrime(input);
        String resultado = (value) ? "PRIME" : "COMPOSITE"
        output == resultado;

        where:
        input | output
        5 | "PRIME"
        12 | "COMPOSITE"
    }

    def "Prueba que valida si es PALINDROMO/NO PALINDROMO"() {
        expect:
        boolean value = MyMath.isPalindrome(input);
        String resultado = (value) ? "PALINDROME" : "NOT PALINDROME"
        output == resultado;

        where:
        input | output
        898 | "PALINDROME"
        133 | "NOT PALINDROME"
    }
}