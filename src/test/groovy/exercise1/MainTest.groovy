package exercise1


import spock.lang.Specification

class MainTest extends Specification {

    def "Obtiene la lista de los numeros al cuadrado y sus pares"() {
        given:
            List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            List<Double> resultadoEsperadoDeListaCuadrado = Arrays.asList(1.0, 4.0, 9.0, 16.0, 25.0, 36.0, 49.0, 64.0, 81.0, 100.0);
            List<Double> resultadoEsperadoDeListaCuadradoPares = Arrays.asList(4.0, 16.0, 36.0, 64.0, 100.0);

        when:
            List<Double> listaCuadrados = Main.obtenerListaCuadrados(numbers);
            List<Double> listaNumerosPares = Main.obtenerListaNroParesDeLosCuadrados(listaCuadrados);

        then:
            resultadoEsperadoDeListaCuadrado == listaCuadrados;
            resultadoEsperadoDeListaCuadradoPares == listaNumerosPares;
    }

}
