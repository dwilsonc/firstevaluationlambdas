package exercise2

import spock.lang.Specification
import spock.lang.Unroll


class MainTest extends Specification {

    def "Obtiene la lista de string en Mayusculas"() {
        given:
            List<String> stringList = Arrays.asList("juan", "manzana", "paralelepipedo", "pedro", "piña", "senior developer", "amargado");
            List<String> resultadoEsperado = Arrays.asList("JUAN", "MANZANA", "PARALELEPIPEDO", "PEDRO", "PIÑA", "SENIOR DEVELOPER", "AMARGADO");

        when:
            List<String> resultado = Main.convertirListadoStringEnMayusculas(stringList);

        then:
            resultadoEsperado == resultado;
    }

    def "Obtiene el promedio de la lista"() {
        given:
            List<Integer> integerList = Arrays.asList(56, 61, 8, 21, 50, 30, 110, 784, 88);
            Double resultadoEsperado = 134.22222222222223;

        when:
            Double resultado = Main.obtenerPromedioDeListaEnteros(integerList);

        then:
            resultadoEsperado == resultado;
    }
}