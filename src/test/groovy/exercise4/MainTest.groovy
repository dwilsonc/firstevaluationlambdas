package exercise4

import spock.lang.Specification
import spock.lang.Unroll


class MainTest extends Specification {

    @Unroll
    def "Obtiene la nueva lista de strings concatenada con numeros y letras"() {
        expect:
        String resultado = Main.obtenerCadenaFinal(input);
        output == resultado

        where:
        input | output
        [21, 730, 840, 221, 14, 13, 729, 1053, 459] | "o21, e730, e840, o221, e14, o13, o729, o1053, o459"
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] | "o1, e2, o3, e4, o5, e6, o7, e8, o9, e10"
        [11, 12 ,13 , 14, 15, 16, 17, 18 , 19, 20] | "o11, e12, o13, e14, o15, e16, o17, e18, o19, e20"
    }
}