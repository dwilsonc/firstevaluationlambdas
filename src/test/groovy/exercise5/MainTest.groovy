package exercise5

import exercise5.models.Employee
import exercise5.utils.PopulateModel
import spock.lang.Specification


class MainTest extends Specification {

    def "Obtiene el promedio de edades de los empleados"() {
        given:
        List<Employee> employeeList = PopulateModel.populateEmployeeModel();
        Double resultadoEsperado = 30.571428571428573;

        when:
        OptionalDouble resultado = Main.obtenerPromedioEdadesDeEmpleados(employeeList);

        then:
        resultadoEsperado == resultado.getAsDouble();
    }
}