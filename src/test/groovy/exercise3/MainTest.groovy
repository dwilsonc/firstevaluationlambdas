package exercise3

import spock.lang.Specification
import spock.lang.Unroll


class MainTest extends Specification {

    @Unroll
    def "Obtiene la lista de string filtrado por letra a en minuscula y palabra de longitud=3"() {
        expect:
            List<String> resultado = Main.obtenerListaDeStringsPorFiltro(input);
            output == resultado

        where:
            input | output
            ["arandanos", "ana", "pepino", "arpegio", "gato", "pepe", "ala"] | ["ana", "ala"]
            ["arandanos", "ana", "api", "arpegio", "gato", "pepe", "ala"] | ["ana", "api", "ala"]
            ["arandanos", "ana", "api", "arpegio", "gato", "ama", "ala"] | ["ana", "api", "ama","ala"]
    }
}