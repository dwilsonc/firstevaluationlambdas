package exercise2;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class Main {

    /**
     * Escriba un método que reciba como parámetro una lista de Strings y retorne la misma lista, pero esta vez en UpperCase (usando Lambdas).
     *
     * Escriba un método que retorna el promedio de una lista de enteros (usando Lambdas).
     *
     * Puntaje total: 10 puntos
     */
    public static void main(String[] args) {
        List<String> stringList = Arrays.asList("juan", "manzana", "paralelepipedo", "pedro", "piña", "senior developer", "amargado");
        List<Integer> integerList = Arrays.asList(56, 61, 8, 21, 50, 30, 110, 784, 88);

        System.out.println("\n== Lista de Strings UpperCase ==");

        Main.convertirListadoStringEnMayusculas(stringList);

        System.out.println("\n\n== Promedio ==");

        Double promedio = Main.obtenerPromedioDeListaEnteros(integerList);

        System.out.println("\n-> " + promedio);
    }

    public static List<String> convertirListadoStringEnMayusculas(List<String> stringList){
        return stringList.stream()
                .map(s -> {
                    return s.toUpperCase();
                })
                .peek(e -> System.out.print("\n-> " + e)) //Solo para mostrar en consola
                .collect(Collectors.toList());
    }

    public static Double obtenerPromedioDeListaEnteros(List<Integer> integerList){
        return integerList
                .stream()
                .collect(
                        Collectors.averagingDouble(num -> Double.valueOf(num)));
    }
}
