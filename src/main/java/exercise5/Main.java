package exercise5;

import exercise5.models.Employee;
import exercise5.utils.PopulateModel;

import java.util.List;
import java.util.OptionalDouble;

public class Main {

    /**
     * Haga una función lambda que permita de una lista de empleados, obtener el promedio de las edades de éstos.
     *
     * Puntaje total: 10 puntos
     */
    public static void main(String[] args) {
        List<Employee> employeeList = PopulateModel.populateEmployeeModel();

        System.out.println("\n== Promedio de edades ==");

        OptionalDouble resultado  = Main.obtenerPromedioEdadesDeEmpleados(employeeList);

        if (resultado.isPresent()){
            System.out.println("\n-> " + resultado.getAsDouble());
        }
    }

    public static OptionalDouble obtenerPromedioEdadesDeEmpleados(List<Employee> employeeList){
        return employeeList
                .stream()
                .mapToDouble(e -> e.getAge())
                .average();
    }
}
