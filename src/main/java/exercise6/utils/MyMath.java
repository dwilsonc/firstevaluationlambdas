package exercise6.utils;

import exercise6.interfaces.PerformOperation;
import lombok.NoArgsConstructor;

import java.util.stream.IntStream;

@NoArgsConstructor
public class MyMath {
    public static boolean checker(PerformOperation p, int num) {
        return p.check(num);
    }

    public PerformOperation isOdd() {
        return num -> isOdd(num);
    }

    public static boolean isOdd (int number) {
        return (number % 2 != 0);
    }

    public PerformOperation isPalindrome() {
        return MyMath::isPalindrome;
    }

    private static boolean isPalindrome(int num) {
        String original = String.valueOf(num);
        int i = original.length()-1;
        int j=0;
        while(i > j) {
            if(original.charAt(i) != original.charAt(j)) {
                return false;
            }
            i--;
            j++;
        }
        return true;
    }

    public PerformOperation isPrime() {
        return num -> isPrime(num);
    }

    public static boolean isPrime(int number) {
        return !IntStream.rangeClosed(2, number/2).anyMatch(i -> number%i == 0);
    }
}
