package exercise6.interfaces;

//@FunctionalInterface
public interface PerformOperation {
    boolean check(int a);
}
