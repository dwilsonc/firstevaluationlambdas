package exercise1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    /**
     * Escriba una función lambda que permita:
     *
     * 1.- Calcular los cuadrados de los números en la lista.
     * 2.- Obtener los números pares en la lista de cuadrados.
     *
     * Puntaje total: 10 puntos
     */
    public static void main(String[] args) {

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        System.out.println("\n== Lista de cuadrados ==");

        List<Double> listaCuadrados = Main.obtenerListaCuadrados(numbers);

        System.out.println("\n\n== Lista numeros pares de los cuadrados ==");

        Main.obtenerListaNroParesDeLosCuadrados(listaCuadrados);
    }

    public static List<Double> obtenerListaCuadrados(List<Integer> numbers){
        return numbers
                .stream()
                .map(num -> {
                    return Math.pow(num,2);
                })
                .peek(e -> System.out.print("\n-> " + e)) //Solo para mostrar en consola
                .collect(Collectors.toList());
    }

    public static List<Double> obtenerListaNroParesDeLosCuadrados(List<Double> listaCuadrados){
        return listaCuadrados
                .stream()
                .filter(num -> (num % 2 == 0))
                .peek(e -> System.out.print("\n-> " + e)) //Solo para mostrar en consola
                .collect(Collectors.toList());
    }
}
