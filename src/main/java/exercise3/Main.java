package exercise3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    /**
     * Dada una lista de Strings, escriba un método que devuelva una lista de todos los Strings que comienzan con la
     * letra 'a' (minúscula) y tienen exactamente 3 letras. SUGERENCIA: Utilice las API de Java 8 Lambdas y Streams
     *
     * Puntaje total: 10 puntos
     */
    public static void main(String[] args) {
        List<String> stringList = Arrays.asList("arandanos", "ana", "pepino", "arpegio", "gato", "pepe", "ala");

        System.out.println("\n== Resultado filtrado ==");

        Main.obtenerListaDeStringsPorFiltro(stringList);
    }

    public static List<String> obtenerListaDeStringsPorFiltro(List<String> stringList){
        return stringList
                .stream()
                .filter(s -> s.startsWith("a") && s.length()==3)
                .peek(e -> System.out.print("\n-> " + e)) //Solo para mostrar en consola
                .collect(Collectors.toList());
    }
}
